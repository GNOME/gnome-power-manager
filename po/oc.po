# Translation of oc.po to Occitan
# Occitan translation of gnome-power-manager.
# Copyright (C) 2005-2007 Free Software Foundation, Inc.
# This file is distributed under the same license as the gnome-power-manager package.
# Yannig Marchegay (Kokoyaya) <yannig@marchegay.org>, 2006-2008.
# Cédric Valmary (totenoc.eu) <cvalmary@yahoo.fr>, 2016.
msgid ""
msgstr ""
"Project-Id-Version: oc\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-power-manager/"
"issues\n"
"POT-Creation-Date: 2021-05-13 15:33+0000\n"
"PO-Revision-Date: 2021-08-18 19:45+0200\n"
"Last-Translator: Quentin PAGÈS\n"
"Language-Team: Tot En Òc\n"
"Language: oc\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Poedit 3.0\n"
"X-Launchpad-Export-Date: 2015-05-20 15:20+0000\n"
"X-Project-Style: gnome\n"

#: data/appdata/org.gnome.PowerStats.appdata.xml.in:7
msgid "GNOME Power Statistics"
msgstr "Estatisticas de l'alimentacion GNOME"

#: data/appdata/org.gnome.PowerStats.appdata.xml.in:8
#: data/org.gnome.PowerStats.desktop.in.in:4
msgid "Observe power management"
msgstr "Susvellha la gestion d'energia"

#: data/appdata/org.gnome.PowerStats.appdata.xml.in:10
msgid ""
"Power Statistics can show historical and current battery information and "
"programs waking up that use power."
msgstr ""
"Las estatisticas d'alimentacion podon mostrar las informacions d'istoricas e "
"actualas tocant la batariá e los programas qu'utilizan l'alimentacion "
"electrica."

#: data/appdata/org.gnome.PowerStats.appdata.xml.in:14
msgid ""
"You probably only need to install this application if you are having "
"problems with your laptop battery, or are trying to work out what programs "
"are using significant amounts of power."
msgstr ""
"Vos cal probablament installar aquesta aplicacion s'avètz de problèmas amb "
"la batariá de l'ordenador portable o se cercatz los programas qu'utilizan un "
"fum d'energia."

#: data/org.gnome.power-manager.gschema.xml:5
msgid "Whether we should show the history data points"
msgstr "Indica se los punts de donadas de l'istoric an d'èsser afichats"

#: data/org.gnome.power-manager.gschema.xml:6
msgid ""
"Whether we should show the history data points in the statistics window."
msgstr ""
"Indica se los punts de donadas de l'istoric an d'èsser afichats dins la "
"fenèstra de las estatisticas."

#: data/org.gnome.power-manager.gschema.xml:10
msgid "Whether we should smooth the history data"
msgstr "Indica se las donadas de l'istoric an d'èsser lissadas"

#: data/org.gnome.power-manager.gschema.xml:11
msgid "Whether we should smooth the history data in the graph."
msgstr "Indica se las donadas de l'istoric an d'èsser lissadas sul grafic."

#: data/org.gnome.power-manager.gschema.xml:15
msgid "The default graph type to show for history"
msgstr "Lo tipe de grafic per defaut d'afichar per l'istoric"

#: data/org.gnome.power-manager.gschema.xml:16
msgid "The default graph type to show in the history window."
msgstr "Lo tipe de grafic per defaut d'afichar dins la fenèstra de l'istoric."

#: data/org.gnome.power-manager.gschema.xml:20
msgid "The maximum time displayed for history"
msgstr "Durada maximala afichada sus l'istoric"

#: data/org.gnome.power-manager.gschema.xml:21
msgid ""
"The maximum duration of time displayed on the x-axis of the history graph."
msgstr ""
"Durada maximala afichada sus l'axe de las abscissas del grafic de l'istoric."

#: data/org.gnome.power-manager.gschema.xml:25
msgid "Whether we should show the stats data points"
msgstr "Indica se los punts de las estatisticas an d'èsser afichats"

#: data/org.gnome.power-manager.gschema.xml:26
msgid "Whether we should show the stats data points in the statistics window."
msgstr ""
"Indica se los punts de las estatisticas an d'èsser afichats dins la fenèstra "
"de las estatisticas."

#: data/org.gnome.power-manager.gschema.xml:30
msgid "Whether we should smooth the stats data"
msgstr "Indica se las donadas de las estatisticas an d'èsser lissadas"

#: data/org.gnome.power-manager.gschema.xml:31
msgid "Whether we should smooth the stats data in the graph."
msgstr ""
"Indica se las donadas de las estatisticas an d'èsser lissadas sul grafic."

#: data/org.gnome.power-manager.gschema.xml:35
msgid "The default graph type to show for stats"
msgstr "Lo tipe de grafic per defaut d'afichar per las estatisticas"

#: data/org.gnome.power-manager.gschema.xml:36
msgid "The default graph type to show in the stats window."
msgstr ""
"Lo tipe de grafic per defaut d'afichar dins la fenèstra de las estatisticas."

#: data/org.gnome.power-manager.gschema.xml:40
msgid "The index of the page number to show by default"
msgstr "L'indici del numèro de pagina d'afichar per defaut"

#: data/org.gnome.power-manager.gschema.xml:41
msgid ""
"The index of the page number to show by default which is used to return "
"focus to the correct page."
msgstr ""
"L'indici del numèro de pagina d'afichar per defaut qu'es utilizat per "
"redonar lo focus a la bona pagina."

#: data/org.gnome.power-manager.gschema.xml:45
msgid "The ID of the last device selected"
msgstr "L'ID del darrièr periferic seleccionat"

#: data/org.gnome.power-manager.gschema.xml:46
msgid ""
"The identifier of the last device which is used to return focus to the "
"correct device."
msgstr ""
"L'identificant del darrièr periferic qu'es utilizat per redonar lo focus cap "
"al bon periferic."

#. TRANSLATORS: shown on the titlebar
#. TRANSLATORS: the program name
#: data/org.gnome.PowerStats.desktop.in.in:3 src/gpm-statistics.c:982
#: src/gpm-statistics.c:1350 src/gpm-statistics.ui:8
msgid "Power Statistics"
msgstr "Estatisticas de l'alimentacion"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.PowerStats.desktop.in.in:6
msgid "battery;consumption;charge;"
msgstr "batariá;consum;carga;"

#. Translators: This is %i days
#: src/egg-graph-widget.c:408
#, c-format
msgid "%id"
msgstr "%i j"

#. Translators: This is %i days %02i hours
#: src/egg-graph-widget.c:411
#, c-format
msgid "%id%02ih"
msgstr "%i j %02i h"

#. Translators: This is %i hours
#: src/egg-graph-widget.c:416
#, c-format
msgid "%ih"
msgstr "%i o"

#. Translators: This is %i hours %02i minutes
#: src/egg-graph-widget.c:419
#, c-format
msgid "%ih%02im"
msgstr "%i h %02i m"

#. Translators: This is %2i minutes
#: src/egg-graph-widget.c:424
#, c-format
msgid "%2im"
msgstr "%2i m"

#. Translators: This is %2i minutes %02i seconds
#: src/egg-graph-widget.c:427
#, c-format
msgid "%2im%02i"
msgstr "%2i m %02i"

#. TRANSLATORS: This is ms
#: src/egg-graph-widget.c:431
#, c-format
msgid "%.0fms"
msgstr "%.0fms"

#. Translators: This is %2i seconds
#: src/egg-graph-widget.c:434
#, c-format
msgid "%2is"
msgstr "%2is"

#. TRANSLATORS: This is %i Percentage
#: src/egg-graph-widget.c:438
#, c-format
msgid "%i%%"
msgstr "%i %%"

#. TRANSLATORS: This is %.1f Watts
#: src/egg-graph-widget.c:441
#, c-format
msgid "%.1fW"
msgstr "%.1f W"

#. TRANSLATORS: This is %.1f Volts
#: src/egg-graph-widget.c:446
#, c-format
msgid "%.1fV"
msgstr "%.1f V"

#. TRANSLATORS: This is %.1f nanometers
#: src/egg-graph-widget.c:449
#, c-format
msgid "%.0f nm"
msgstr "%.0f nm"

#. TRANSLATORS: the rate of discharge for the device
#: src/gpm-statistics.c:72 src/gpm-statistics.c:634
msgid "Rate"
msgstr "Taus"

#: src/gpm-statistics.c:73
msgid "Charge"
msgstr "Carga"

#: src/gpm-statistics.c:74 src/gpm-statistics.c:648
msgid "Time to full"
msgstr "Durada de la carga"

#: src/gpm-statistics.c:75 src/gpm-statistics.c:653
msgid "Time to empty"
msgstr "Durada de la descarga"

#: src/gpm-statistics.c:82
msgid "30 minutes"
msgstr "30 minutas"

#: src/gpm-statistics.c:83
msgid "3 hours"
msgstr "3 oras"

#: src/gpm-statistics.c:84
msgid "8 hours"
msgstr "8 oras"

#: src/gpm-statistics.c:85
msgid "1 day"
msgstr "1 jorn"

#: src/gpm-statistics.c:86
msgid "1 week"
msgstr "1 setmana"

#. 5 min tick
#. 30 min tick
#. 1 hr tick
#. 2 hr tick
#. 1 day tick
#. TRANSLATORS: what we've observed about the device
#: src/gpm-statistics.c:101
msgid "Charge profile"
msgstr "Perfil de carga"

#: src/gpm-statistics.c:102
msgid "Discharge profile"
msgstr "Perfil de descarga"

#. TRANSLATORS: how accurately we can predict the time remaining of the battery
#: src/gpm-statistics.c:104
msgid "Charge accuracy"
msgstr "Precision de la carga"

#: src/gpm-statistics.c:105
msgid "Discharge accuracy"
msgstr "Precision de la descarga"

#. TRANSLATORS: system power cord
#: src/gpm-statistics.c:235
msgid "AC adapter"
msgid_plural "AC adapters"
msgstr[0] "Adaptador sector"
msgstr[1] "Adaptadors sector"

#. TRANSLATORS: laptop primary battery
#: src/gpm-statistics.c:239
msgid "Laptop battery"
msgid_plural "Laptop batteries"
msgstr[0] "Batariá de l'ordenador"
msgstr[1] "Batariás de l'ordenador"

#. TRANSLATORS: battery-backed AC power source
#: src/gpm-statistics.c:243
msgid "UPS"
msgid_plural "UPSs"
msgstr[0] "Ondulador"
msgstr[1] "Onduladors"

#. TRANSLATORS: a monitor is a device to measure voltage and current
#: src/gpm-statistics.c:247
msgid "Monitor"
msgid_plural "Monitors"
msgstr[0] "Ecran"
msgstr[1] "Ecrans"

#. TRANSLATORS: wireless mice with internal batteries
#: src/gpm-statistics.c:251
msgid "Mouse"
msgid_plural "Mice"
msgstr[0] "Mirga"
msgstr[1] "Mirgas"

#. TRANSLATORS: wireless keyboard with internal battery
#: src/gpm-statistics.c:255
msgid "Keyboard"
msgid_plural "Keyboards"
msgstr[0] "Clavièr"
msgstr[1] "Clavièrs"

#. TRANSLATORS: portable device
#: src/gpm-statistics.c:259
msgid "PDA"
msgid_plural "PDAs"
msgstr[0] "Assistent personal"
msgstr[1] "Assistents personals"

#. TRANSLATORS: cell phone (mobile...)
#: src/gpm-statistics.c:263
msgid "Cell phone"
msgid_plural "Cell phones"
msgstr[0] "Telefonet"
msgstr[1] "Telefonets"

#. TRANSLATORS: media player, mp3 etc
#: src/gpm-statistics.c:268
msgid "Media player"
msgid_plural "Media players"
msgstr[0] "Lector multimèdia"
msgstr[1] "Lectors multimèdia"

#. TRANSLATORS: tablet device
#: src/gpm-statistics.c:272
msgid "Tablet"
msgid_plural "Tablets"
msgstr[0] "Tableta"
msgstr[1] "Tabletas"

#. TRANSLATORS: tablet device
#: src/gpm-statistics.c:276
msgid "Computer"
msgid_plural "Computers"
msgstr[0] "Ordenador"
msgstr[1] "Ordenadors"

#. TRANSLATORS: battery technology
#: src/gpm-statistics.c:293
msgid "Lithium Ion"
msgstr "Liti-ion"

#. TRANSLATORS: battery technology
#: src/gpm-statistics.c:297
msgid "Lithium Polymer"
msgstr "Liti polimèr"

#. TRANSLATORS: battery technology
#: src/gpm-statistics.c:301
msgid "Lithium Iron Phosphate"
msgstr "Liti fosfat de fèrre"

#. TRANSLATORS: battery technology
#: src/gpm-statistics.c:305
msgid "Lead acid"
msgstr "Batariá al plomb"

#. TRANSLATORS: battery technology
#: src/gpm-statistics.c:309
msgid "Nickel Cadmium"
msgstr "Niquèl Cadmi"

#. TRANSLATORS: battery technology
#: src/gpm-statistics.c:313
msgid "Nickel metal hydride"
msgstr "Niquèl metal idrur"

#. TRANSLATORS: battery technology
#: src/gpm-statistics.c:317
msgid "Unknown technology"
msgstr "Tecnologia desconeguda"

#. TRANSLATORS: battery state
#: src/gpm-statistics.c:334
msgid "Charging"
msgstr "En carga"

#. TRANSLATORS: battery state
#: src/gpm-statistics.c:338
msgid "Discharging"
msgstr "En descarga"

#. TRANSLATORS: battery state
#: src/gpm-statistics.c:342
msgid "Empty"
msgstr "Void"

#. TRANSLATORS: battery state
#: src/gpm-statistics.c:346
msgid "Charged"
msgstr "Cargat"

#. TRANSLATORS: battery state
#: src/gpm-statistics.c:350
msgid "Waiting to charge"
msgstr "En espèra de carga"

#. TRANSLATORS: battery state
#: src/gpm-statistics.c:354
msgid "Waiting to discharge"
msgstr "En espèra de descarga"

#. TRANSLATORS: battery state
#. TRANSLATORS: this is when the stats time is not known
#: src/gpm-statistics.c:358 src/gpm-statistics.c:472
msgid "Unknown"
msgstr "Desconegut"

#: src/gpm-statistics.c:375
msgid "Attribute"
msgstr "Atribut"

#: src/gpm-statistics.c:382
msgid "Value"
msgstr "Valor"

#: src/gpm-statistics.c:396
msgid "Image"
msgstr "Imatge"

#: src/gpm-statistics.c:402
msgid "Description"
msgstr "Descripcion"

#. TRANSLATORS: this is a time value, usually to show on a graph
#: src/gpm-statistics.c:476
#, c-format
msgid "%.0f second"
msgid_plural "%.0f seconds"
msgstr[0] "%.0f segonda"
msgstr[1] "%.0f segondas"

#. TRANSLATORS: this is a time value, usually to show on a graph
#: src/gpm-statistics.c:481
#, c-format
msgid "%.1f minute"
msgid_plural "%.1f minutes"
msgstr[0] "%.1f minuta"
msgstr[1] "%.1f minutas"

#. TRANSLATORS: this is a time value, usually to show on a graph
#: src/gpm-statistics.c:486
#, c-format
msgid "%.1f hour"
msgid_plural "%.1f hours"
msgstr[0] "%.1f ora"
msgstr[1] "%.1f oras"

#. TRANSLATORS: this is a time value, usually to show on a graph
#: src/gpm-statistics.c:490
#, c-format
msgid "%.1f day"
msgid_plural "%.1f days"
msgstr[0] "%.1f jorn"
msgstr[1] "%.1f jorns"

#: src/gpm-statistics.c:496
msgid "Yes"
msgstr "Òc"

#: src/gpm-statistics.c:496
msgid "No"
msgstr "Non"

#. TRANSLATORS: the device ID of the current device, e.g. "battery0"
#: src/gpm-statistics.c:571
msgid "Device"
msgstr "Periferic"

#: src/gpm-statistics.c:573
msgid "Type"
msgstr "Tipe"

#: src/gpm-statistics.c:575
msgid "Vendor"
msgstr "Vendeire"

#: src/gpm-statistics.c:577
msgid "Model"
msgstr "Modèl"

#: src/gpm-statistics.c:579
msgid "Serial number"
msgstr "Numèro de seria"

#. TRANSLATORS: a boolean attribute that means if the device is supplying the
#. * main power for the computer. For instance, an AC adapter or laptop battery
#. * would be TRUE,  but a mobile phone or mouse taking power is FALSE
#: src/gpm-statistics.c:584
msgid "Supply"
msgstr "Alimentacion"

#: src/gpm-statistics.c:587
#, c-format
msgid "%u second"
msgid_plural "%u seconds"
msgstr[0] "%u segonda"
msgstr[1] "%u segondas"

#. TRANSLATORS: when the device was last updated with new data. It's
#. * usually a few seconds when a device is discharging or charging.
#: src/gpm-statistics.c:591
msgid "Refreshed"
msgstr "Actualizat"

#. TRANSLATORS: Present is whether the device is currently attached
#. * to the computer, as some devices (e.g. laptop batteries) can
#. * be removed, but still observed as devices on the system
#: src/gpm-statistics.c:601
msgid "Present"
msgstr "Present(a)"

#. TRANSLATORS: If the device can be recharged, e.g. lithium
#. * batteries rather than alkaline ones
#: src/gpm-statistics.c:608
msgid "Rechargeable"
msgstr "Recargable"

#. TRANSLATORS: The state of the device, e.g. "Changing" or "Fully charged"
#: src/gpm-statistics.c:614
msgid "State"
msgstr "Estat"

#: src/gpm-statistics.c:618
msgid "Energy"
msgstr "Energia"

#: src/gpm-statistics.c:621
msgid "Energy when empty"
msgstr "Energia a void"

#: src/gpm-statistics.c:624
msgid "Energy when full"
msgstr "Energia a plena carga"

#: src/gpm-statistics.c:627
msgid "Energy (design)"
msgstr "Energia (teorica)"

#: src/gpm-statistics.c:641
msgid "Voltage"
msgstr "Tension"

#. TRANSLATORS: the amount of charge the cell contains
#: src/gpm-statistics.c:663
msgid "Percentage"
msgstr "Percentatge"

#. TRANSLATORS: the capacity of the device, which is basically a measure
#. * of how full it can get, relative to the design capacity
#: src/gpm-statistics.c:670
msgid "Capacity"
msgstr "Capacitat"

#. TRANSLATORS: the type of battery, e.g. lithium or nikel metal hydroxide
#: src/gpm-statistics.c:675
msgid "Technology"
msgstr "Tecnologia"

#. TRANSLATORS: this is when the device is plugged in, typically
#. * only shown for the ac adaptor device
#: src/gpm-statistics.c:680
msgid "Online"
msgstr "En linha"

#. TRANSLATORS: shown on the titlebar
#: src/gpm-statistics.c:974
msgid "Device Information"
msgstr "Informacions sul periferic"

#. TRANSLATORS: shown on the titlebar
#: src/gpm-statistics.c:976
msgid "Device History"
msgstr "Istoric del periferic"

#. TRANSLATORS: shown on the titlebar
#: src/gpm-statistics.c:978
msgid "Device Profile"
msgstr "Perfil del periferic"

#. TRANSLATORS: this is the X axis on the graph
#: src/gpm-statistics.c:1142 src/gpm-statistics.c:1148
#: src/gpm-statistics.c:1154 src/gpm-statistics.c:1160
msgid "Time elapsed"
msgstr "Temps"

#. TRANSLATORS: this is the Y axis on the graph
#: src/gpm-statistics.c:1144
msgid "Power"
msgstr "Poténcia"

#. TRANSLATORS: this is the Y axis on the graph for the whole battery device
#. TRANSLATORS: this is the X axis on the graph for the whole battery device
#: src/gpm-statistics.c:1150 src/gpm-statistics.c:1189
#: src/gpm-statistics.c:1195 src/gpm-statistics.c:1201
#: src/gpm-statistics.c:1207
msgid "Cell charge"
msgstr "Carga de la batariá"

#. TRANSLATORS: this is the Y axis on the graph
#: src/gpm-statistics.c:1156 src/gpm-statistics.c:1162
msgid "Predicted time"
msgstr "Durada prevista"

#. TRANSLATORS: this is the Y axis on the graph
#: src/gpm-statistics.c:1191 src/gpm-statistics.c:1203
msgid "Correction factor"
msgstr "Factor de correccion"

#. TRANSLATORS: this is the Y axis on the graph
#: src/gpm-statistics.c:1197 src/gpm-statistics.c:1209
msgid "Prediction accuracy"
msgstr "Precision de la prediccion"

#. TRANSLATORS: show verbose debugging
#: src/gpm-statistics.c:1338
msgid "Show extra debugging information"
msgstr "Aficha las informacions de desbugatge"

#. TRANSLATORS: show a device by default
#: src/gpm-statistics.c:1341
msgid "Select this device at startup"
msgstr "Seleccionar aqueste periferic a l'aviada"

#: src/gpm-statistics.ui:66
msgid "Details"
msgstr "Detalhs"

#: src/gpm-statistics.ui:92 src/gpm-statistics.ui:296
msgid "Graph type:"
msgstr "Tipe de grafic :"

#: src/gpm-statistics.ui:127
msgid "Data length:"
msgstr "Longor de las donadas :"

#: src/gpm-statistics.ui:186 src/gpm-statistics.ui:348
msgid "There is no data to display."
msgstr "I a pas de donadas d'afichar."

#: src/gpm-statistics.ui:228 src/gpm-statistics.ui:391
msgid "Use smoothed line"
msgstr "Lissar la corba"

#: src/gpm-statistics.ui:244 src/gpm-statistics.ui:407
msgid "Show data points"
msgstr "Afichar los punts de donadas"

#: src/gpm-statistics.ui:274
msgid "History"
msgstr "Istoric"

#: src/gpm-statistics.ui:437
msgid "Statistics"
msgstr "Estatisticas"

#~ msgid "6 hours"
#~ msgstr "6 oras"

#~ msgid "Processor wakeups per second:"
#~ msgstr "Nombre de revelhs del processor per segonda :"

#~ msgid "0"
#~ msgstr "0"

#~ msgid "Wakeups"
#~ msgstr "Revelhs"

#~ msgid "ID"
#~ msgstr "Identificant (ID)"

#~ msgid "Command"
#~ msgstr "Comanda"

#~ msgid "No data"
#~ msgstr "Pas de donada"

#~ msgid "Kernel module"
#~ msgstr "Modul del nucli"

#~ msgid "Kernel core"
#~ msgstr "Nucli central"

#~ msgid "Interprocessor interrupt"
#~ msgstr "Interrupcion entre processors"

#~ msgid "Interrupt"
#~ msgstr "Interrupcion"

#~ msgid "PS/2 keyboard/mouse/touchpad"
#~ msgstr "Clavièr/mirga/pavat tactil PS/2"

#~ msgid "ACPI"
#~ msgstr "ACPI"

#~ msgid "Serial ATA"
#~ msgstr "ATA seria"

#~ msgid "ATA host controller"
#~ msgstr "Contrarolador d'òste ATA"

#~ msgid "Intel wireless adaptor"
#~ msgstr "Adaptator sens fial Intel"

#~ msgid "Timer %s"
#~ msgstr "Prètfach periodic %s"

#~ msgid "Sleep %s"
#~ msgstr "Prètzfach endormit %s"

#~ msgid "New task %s"
#~ msgstr "Prètzfach novèl %s"

#~ msgid "Wait %s"
#~ msgstr "Prètzfach en espèra %s"

#~ msgid "Work queue %s"
#~ msgstr "Fila d'espèra %s"

#~ msgid "Network route flush %s"
#~ msgstr "Voidatge de las rotas de ret %s"

#~ msgid "USB activity %s"
#~ msgstr "Activitat USB %s"

#~ msgid "Wakeup %s"
#~ msgstr "Despertament de %s"

#~ msgid "Local interrupts"
#~ msgstr "Interrupcions localas"

#~ msgid "Rescheduling interrupts"
#~ msgstr "Interrupcions de reordonançament"

#~ msgid "Processor Wakeups"
#~ msgstr "Despertaments del processor"

#~ msgid "Processor"
#~ msgstr "Processor"
